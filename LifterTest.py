import os
import time

DELAY_TIME = 25
DELAY_TIME2 = 40
COUTER_WAIT = 3

cycle_counter = 0

cocomm_path = '~/CANopenLinux/cocomm/'
canopend_path = '~/CANopenLinux/'
os.environ['PATH'] = f'{cocomm_path}:{cocomm_path}:{os.environ["PATH"]}'

def execute_commands(commands):
    for command in commands:
        return_code = os.system(command)

        # Печать кода возврата для каждой команды
        #if return_code != 0:
        #    print(f'Ошибка выполнения команды "{command}". Код возврата: {return_code}')

command_StartCanOpen = [
    'rm "/tmp/CO_command_socket"',
    'canopend can0 -i 55 -c "local-/tmp/CO_command_socket" &'
]

command_StartCan = [
    'ip link set can0 type can bitrate 500000',
    'ip link set can0 up'
]

command_Emerg = [
    'cocomm "13 w 0x6405 0 u8 1"'
]


command_LiftOff = [
    'cocomm "13 w 0x6402 0 u8 0"'
]

command_LiftUp = [
    'echo -n " UP " && cocomm "13 w 0x6402 0 u8 1"'
]

command_LiftDown = [
    'echo -n " DOWN " && cocomm "13 w 0x6402 0 u8 2"'
]

command_ReadCur1NotFiltered = [
    'echo -n " Current 1 (not filtered): " && cocomm "13 r 0x640A 1 u32"'
]

command_ReadCur2NotFiltered = [
    'echo -n " Current 2 (not filtered): " && cocomm "13 r 0x640A 2 u32"'
]

command_ReadCur1Filtered = [
    'echo -n " Current 1 (filtered): " && cocomm "13 r 0x640A 3 u32"'
]

command_ReadCur2Filtered = [
    'echo -n " Current 2 (filtered): " && cocomm "13 r 0x640A 4 u32"'
]

command_GetStatus = [
    'echo -n " Status: " && cocomm "13 r 0x6404 0 u32"'
]


def print_current():
    execute_commands(command_ReadCur1NotFiltered)
    execute_commands(command_ReadCur2NotFiltered)
    execute_commands(command_ReadCur1Filtered)
    execute_commands(command_ReadCur2Filtered)
    

execute_commands(command_StartCanOpen)
#execute_commands(command_Emerg)


while True:
    execute_commands(command_LiftUp)
    time.sleep(DELAY_TIME)
    print_current()
    execute_commands(command_GetStatus)
    
    execute_commands(command_LiftDown)
    time.sleep(DELAY_TIME)
    print_current()
    execute_commands(command_GetStatus)

    cycle_counter += 1

    if cycle_counter >= COUTER_WAIT:
        print(f"Waiting for {DELAY_TIME2} seconds...")
        time.sleep(DELAY_TIME2)
        cycle_counter = 0
